package com.example.canvasdraewingdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageview = findViewById(R.id.imageview);
        Bitmap b = Bitmap.createBitmap(300, 500, Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(b);
        Paint paintbrush = new Paint();


        canvas.drawColor(Color.BLACK);
        paintbrush.setColor(Color.YELLOW);
        canvas.drawLine(10,50,200,30, paintbrush);
        canvas.drawLine(20,50,150,50, paintbrush);
        paintbrush.setColor(Color.WHITE);
        canvas.drawRect(100,100, 200,200 , paintbrush);

    }
}
